import numpy as np
from numpy.polynomial import polynomial as P
import pylab

"""
p(-4) = 16
p(-2) = 4
p(2) = 4
p(4) = 16

A*X=B
X = (A**-1)B
"""

X = np.array([50., 350., 600.])
B = np.array([1., 5., 10.])


def najmanji_kvadrati(x, y):
    n = len(y)

    suma_x = sum(x)
    suma_y = sum(y)
    suma_x2 = sum([x_i**2 for x_i in x])
    suma_x3 = sum([x_i**3 for x_i in x])
    suma_x4 = sum([x_i**4 for x_i in x])
    suma_xy = sum([x[i]*y[i] for i in range(n)])
    suma_x2y = sum([(x[i]**2)*y[i] for i in range(n)])

    a = np.array([[float(n), float(suma_x), float(suma_x2)], [float(suma_x), float(suma_x2), float(suma_x3)], [float(suma_x2), float(suma_x3), float(suma_x4)]])
    b = np.array([suma_y, suma_xy, suma_x2y])

    return np.linalg.solve(a, b)

def regresijski_pravac(X, y, deg=2):
    koef = np.polyfit(X, y, deg)
    reg_pravac = np.poly1d(koef)
    return reg_pravac


def interp_coeff(X, Y):
    n = len(X)

    A = np.zeros(shape=(n, n))
    for i in range(n):
        for j in range(n):
            A[i][j] = X[i]**j

    return np.linalg.solve(A, B)


if __name__ == '__main__':
    x = np.linspace(0, 4000, 10)
    print(x)

    poly = interp_coeff(X, B)
    reg_pravac = najmanji_kvadrati(X, B)

    pylab.figure()
    pylab.xlabel("Price")
    pylab.ylabel("Quantity")
    pylab.grid()
    pylab.plot(x, P.polyval(x, poly), 'o', x, P.polyval(x, reg_pravac), '-')
    pylab.show()

    #print(P.polyval([50., 350., 600., 1200.], poly))
    # np.linalg.solve()
