from math import factorial as fact
import numpy as np

x = np.array([2, 3, 5])

def f(x):
    return np.e**x

y = f(x)
n = len(x)

def newton_coeff(x, y):
    a = np.zeros(3)
    for i in range(n):
        a[i] = y[i]
    
    for j in range(1, n):
        for i in range(n-1, j-1, -1):
            a[i] = (a[i] - a[i-1]) / (x[i] - x[i-j])
    
    return a

def greska(z):
    m3 = np.e**z
    omega = 1.
    for i in range(n):
        omega *= (z - x[i])
    return abs(omega) * m3/fact(n)


print("Koeficijenti NIP-a:", newton_coeff(x, y))
print("Greška u 2.1:", greska(2.1))