# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 21:35:55 2019

@author: Math
"""

from numpy import *
#from scipy import *
#from scipy import integrate
import scipy.integrate as spi
from scipy import interpolate as i
from pylab import *

def f(x):
    return e**x

a = 0.0
b = 3.0
c = (a+b)/2

x1 = array([a, b])
x2 = array([a, c, b])
y1 = f(x1)
y2 = f(x2)

iTrap = spi.trapz(y1, x1)
iSimp = spi.simps(y2, x2)
print('Rjesenje integrala koristeci trapeznu formulu je: ', iTrap)
print('Rjesenje integrala koristeci Simpsonovu formulu je: ', iSimp)

integral = e**3 - e**0
print('Prava vrijednost integrala je: ', integral)
print('Prava greska trapezne formule je: ', abs(integral-iTrap))
print('Prava greska Simpsonove formule je: ', abs(integral-iSimp))

M2 = e**3
print('Apsolutna greska trapezne formule je <= ', (b-a)**3/12.*M2)
M4 = e**3
print('Apsolutna greska Simpsonove formule je <= ', ((b-a)/2.)**5*(M4/90))

pL = i.lagrange(x2, y2)
print('Kvadratni polinom kojim aproksimiramo e**x je: ', pL)

xtocke = linspace(a, b, 50)
plot(xtocke, f(xtocke), x1, y1, xtocke, pL(xtocke))
legend(['funkcija', 'iTrap', 'iSimp'])
fill_between(xtocke, f(xtocke))
show()

