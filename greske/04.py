# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 13:08:00 2019

@author: Korisnik
"""

import math
import decimal as dec

a = dec.Decimal(math.pi)
b = dec.Decimal(math.pi + 10**(-10))

a_r = round(a, 12)
b_r = round(b, 13)

naz = dec.Decimal(a) - dec.Decimal(b)
naz1 = dec.Decimal(a_r) - dec.Decimal(b_r)

aps = abs(dec.Decimal(naz) - dec.Decimal(naz1))
rel = abs(dec.Decimal(aps) / dec.Decimal(naz))

print "Apsolutna greška:", aps
print "Relativna greška:", rel
