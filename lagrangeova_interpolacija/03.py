import pylab
import numpy as np
from numpy.polynomial import polynomial as P

m1 = np.array([[1, 0, 0],[0, 1, 0],[1, 1, 1]])
m2 = np.array([1, 1, 1])

a = np.linalg.solve(m1, m2)

# P.polyval(x, P(y)) rješava polinom a
# tako što uvrštava dani x
print(f'p(0) = {P.polyval(0., a)}')
print(f'p(0) = {P.polyval(0., a)}')
print(f'p(1) = {P.polyval(1., a)}')


x = np.linspace(0.5, 5, 100)
y = P.polyval(x, a)

pylab.plot(x, y)
pylab.show()