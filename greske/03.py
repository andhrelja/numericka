# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 13:03:38 2019

@author: Korisnik
"""

import decimal as dec
from math import pi
from datetime import datetime

start = datetime.now()

pi1 = round(pi, 3)

print "Zaokruzeni pi:", pi1

print "Relativno egzaktni pravi pi:", pi

aps = abs(pi1 - pi)
print "Apsolutna greška je:", aps

rel = abs(pi1 - pi) / abs(pi1)
print "Relativna greška je:", rel

end = datetime.now()
print "Vrijeme:", end-start, "s"