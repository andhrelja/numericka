import numpy as np
import pylab


x = [0., 0.4, 0.8, 1.2, 1.6, 2.0]
y = [0.21, 1.25, 2.31, 2.70, 2.65, 3.20]

koef1 = np.polyfit(x, y, 1)
print("Koeficijenti regresijskog pravca:", koef1)

reg_polinom1 = np.poly1d(koef1)
print("Regresijski polinom stupnja 1:", reg_polinom1)

koef2 = np.polyfit(x, y, 2)
print("Koeficijenti regresijske parabole:", koef2)

reg_polinom2 = np.poly1d(koef2)
print("Regresijski polinom stupnja 2:\n", reg_polinom2)

x.append(1.4)
y.append(reg_polinom2(1.4))

xtocke = np.linspace(min(x) - .5, max(x) + .5, 100)
y_reg_polinom1 = reg_polinom1(xtocke)
y_reg_polinom2 = reg_polinom2(xtocke)

pylab.plot(x, y, 'o', xtocke, y_reg_polinom1, '-g', xtocke, y_reg_polinom2, '-r')
pylab.xlabel("x - os")
pylab.ylabel("y - os")
pylab.show()

print("RegPolinom1(1.4)=", reg_polinom1(1.4))
print("RegPolinom2(1.4)=", reg_polinom2(1.4))