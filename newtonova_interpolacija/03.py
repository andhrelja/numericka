import numpy as np

x = np.array([0, 2, 6])
y = np.array([-1, 2, 3])

n = len(x)

a = []
for i in range(n):
    a.append(y[i])

for j in range(1, n):
    for i in range(n-1, j-1, -1):
        a[i] = (a[i] - a[i-1]) / (x[i] - x[i-j])

print("Koeficijenti Newtonovog interpolacijskog polinoma su:", a)