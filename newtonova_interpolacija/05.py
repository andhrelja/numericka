from math import factorial as fact
import numpy as np

x = np.array([-2, 2, -4])
y = np.array([-5, 3, 211])

n = len(x)

def newton_coeff(x, y):
    a = np.zeros(3)
    for i in range(n):
        a[i] = y[i]
    
    for j in range(1, n):
        for i in range(n-1, j-1, -1):
            a[i] = (a[i] - a[i-1]) / (x[i] - x[i-j])
    
    return a

a = newton_coeff(x, y)
print("Koeficijenti NIP-a:", a)
print(np.linalg.solve(a, 1))