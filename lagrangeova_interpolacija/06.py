import pylab
import numpy as np
from numpy.polynomial import polynomial as P

m1 = np.array([[1, 0, 0],[1, 580, 580**2],[0, 1, 0]])
m2 = np.array([0, 0, 1])

# a je parabola (putanja)
a = np.linalg.solve(m1, m2)
d = P.polyder(a)

print(f'P(0) = {P.polyval(0., a)}')
print(f'P(580) = {P.polyval(580., a)}')
print(f'P\'(0) = {P.polyval(0., d)}')

# 580/2 je sredina puta (parabole)
print("Maksimalna visina je :", P.polyval(580./2, a))