import numpy as np
import pylab
from scipy import integrate

f = lambda y, x: x - y
fun = lambda x: 2 * np.e**x - x - 1

x0 = 0.
y0 = 1.

n = 99
b = 1.
h = (b-x0)/n

xi = np.linspace(0, 1, n+1)
yi = np.zeros(n+1)
yi[0] = 1.

print("Aproksimativne vrijednosti yi:")
for i in range(1, n+1):
    yi[i] = yi[i-1] + h * f(yi[i-1], xi[i-1])

print("Eulerovom metodom dobivamo y99 =", yi[99])
print("Egzaktna vrijednost funkcije u x=1 je:", fun(1.))
print("Apsolutna greška je:", abs(fun(1.) - yi[99]))
