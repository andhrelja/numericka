import numpy as np
from scipy import interpolate
import pylab

def f(x):
    return 1. / (1. + 5*x**2)

xcvorovi = np.linspace(-1, 1, 20)
ycvorovi = f(xcvorovi)

kspline = interpolate.interp1d(xcvorovi, ycvorovi, kind='cubic')
print("Relativna greška u x = 0.95:", abs(f(0.95) - kspline(0.95)) / abs(f(0.95)))

xtocke = np.linspace(min(xcvorovi), max(xcvorovi), 100)

yspline = kspline(xtocke)
yfunkcija = f(xtocke)

pylab.figure()
pylab.plot(xcvorovi, ycvorovi, 'o', xtocke, yspline, '--', xtocke, yfunkcija, '-r')
pylab.legend(['Cvorovi', 'Kspline', 'Funkcija'])
pylab.grid()
pylab.show()