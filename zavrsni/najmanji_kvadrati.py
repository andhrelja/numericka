import numpy as np
from numpy.polynomial import polynomial as P


def najmanji_kvadrati(x, y):
    n = len(y)

    suma_x = sum(x)
    suma_y = sum(y)
    suma_x2 = sum([x_i**2 for x_i in x])
    suma_xy = sum([x[i]*y[i] for i in range(n)])

    a = np.array([[float(n), float(suma_x)], [float(suma_x), float(suma_x2)]])
    b = np.array([suma_y, suma_xy])

    return np.linalg.solve(a, b)

if __name__ == '__main__':
    x = [2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014]
    y = [12.73, 13.15, 13.39, 13.78, 13.11, 11.59, 11.83, 12.56, 12.06, 12.21, 12.1,12.19, 11.49, 12.39, 12.14]

    kv = najmanji_kvadrati(x, y)
    print(np.polyval(kv, 2010))