import numpy as np
import pylab

def f(x):
    return x**5 - x**3 - 2

def df(x):
    return 5*x**4 - 3*x**2

def d2f(x):
    return 20*x**3 - 6*x

#x = np.linspace(-2, 2, 100)
#pylab.plot(x, f(x), 'b-')
#pylab.grid()
#pylab.show()


def Newton(x, eps, max_iteracija=30):
    if f(x) * d2f(x) <= 0:
        print("Nema kovergencije. Birati drugi kraj segmenta.")
    else:
        print("0.-ta iteracija: %s " %x)
        i = 1
        while i <= max_iteracija:
            if abs(df(x)) < eps:
                print("Horizontalna tangenta!")
                break
            dx = f(x) / df(x)
            x = x - dx
            print("%s. iteracija je: %s" %(i, x))
            if abs(dx) < eps:
                return x, i
            else:
                i += 1
        print("Rješenje do na zadanu točnost nije određeno!")

xn, i = Newton(1.5, 1.e-10)
print("x%s = %s" %(i, xn))

pylab.plot(x, f(x), 'b-', xn, f(xn), 'm^')
pylab.grid()
pylab.show()
