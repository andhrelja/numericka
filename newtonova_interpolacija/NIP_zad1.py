from numpy import *
from numpy.polynomial import polynomial as p
import pylab

a=-1.
b=1.
n=2

h=(b-a)/n

polje=[]

for i in range(0, n+1):
    polje.append(a+i*h)

print(polje)

def f(x):
    z=[]
    for i in range(0, n+1):
        z.append(x[i]*cos(x[i]*pi))
    return z

print(f(polje))
y=f(polje)

M1=array([[1,-1,1],[1,0,0],[1,1,1]])
M2=array([1,0,-1])

rj=linalg.solve(M1, M2)
print('Rjesenja:')
print(rj)

print('Provjera uvjeta: ')

for i in range(0, n+1):
    print('p(', polje[i], ')=', p.polyval(polje[i], rj))

print('Aproksimativna vrijednost p(0.4)=', p.polyval(0.4, rj))

def g(x):
    return x*cos(pi*x)

print('Priblizno egzaktna vrijednost g(0.4)=', g(0.4))

print('Relativna greska je: ', abs(g(0.4)-p.polyval(0.4, rj))/abs(g(0.4)))

x=linspace(-2, 2, 100)
y2=x*cos(pi*x)
y11=p.polyval(x, rj)

pylab.plot(x, y11, x, y2)
pylab.show()
