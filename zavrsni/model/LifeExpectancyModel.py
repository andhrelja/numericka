class LifeExpectancy(dict):
    def __init__(self, **kwargs):
        keys = ['country', 'year', 'status', 'life_expectancy', 'adult_mortality', 'infant_deaths', 'alcohol', 'percentage_expenditure', 'hepatitis_b', 'measles', 'bmi', 'under_five_deaths', 'polio', 'total_expenditure', 'diphtheria', 'hiv_aids', 'gdp', 'population', 'thinness__1_19_years', 'thinness_5_9_years', 'income_composition_of_resources', 'schooling']
        for key in keys:
            try:
                kwargs[key]
            except KeyError:
                kwargs[key] = ''
        
        for key, value in kwargs.items():
            if key == "status" and value == 'Developing':
                setattr(self, key, 0)
            elif key == "status" and value == 'Developed':
                setattr(self, key, 1)
            setattr(self, key, value)
        
        self._init()
    
    def _init(self):
        # Cast datatypes
        for key, item in self.__dict__.items():
            if item != "":
                try:
                    setattr(self, key, eval(item))
                except SyntaxError:
                    setattr(self, key, item)
                except NameError:
                    pass
    
    def export_line(self):
        # Export CSV line
        string = str()
        for _, item in self.__dict__.items():
            string += str(item) + ","
        
        # Remove trailing comma
        return string[:-1] + "\n"
    
    def __repr__(self):
        return f'<LifeExpectancy country={self.country} year={self.year}>'
    
    def __str__(self):
        # Print line
        for _, item in self.__dict__.items():
            print(item, end="\t")