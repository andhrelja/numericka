# f(x) = sin(x)
# f(0) = sin(0) = 0
# f(pi) = sin(pi) = 0

# f'(x) = cos(x)
# f'(0) = cos(0) = 1
# f'(pi) = cos(pi) = -1

-------
# p3(x) = ax**3 + bx**2 + cx + d

# p3(0) = 0
# p3(pi) = pi**3a + pi**2b + pic + d = 0
-------
# p3'(x) = 3ax**2 + 2bx + c + 0

# p3'(0) = c = 1
# p3'(pi) = 3pi**2a + 2pib + c = -1

