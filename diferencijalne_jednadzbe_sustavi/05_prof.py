#Na intervalu [1,3] na 100 ekvidistantno udaljenih tocaka nadjite i prikazite
#graficki priblizno rjesenje obicne diferencijalne jednadzbe y'''=6/(x^3).
#Koristite pocetne uvjete: y(1)=2, y'(1)=1, y''(1)=1. Usporedite numericko rjesenje
#s analitickim rjesenjem y=3*lnx+2*x^2-6*x+6.

from scipy import integrate
from numpy import *
from pylab import *

#Funkcija kojom je definiran sustav diferencijalnih jednadzbi
def f(z,x):
    return array([z[1], z[2], 6/(x**3)])

#Analiticko rjesenje
def fun(x):
    return 3*log(x)+2*x**2-6*x+6

#Pocetni uvjeti
y0=array([2.,1.,1.])

#Određivanje 100 ekvidistantnih tocaka na segmentu [0,2]
xi=linspace(1,3,100)

#Rjesenje
yi=integrate.odeint(f,y0,xi,atol=1.e-12,rtol=1.e-12)
print('Aproksimativne vrijednosti yi:')
for i in range(0,100):
    print('y%d=%.15f' % (i,yi[i,0]))
print('Vrijednost funkcije y=3*lnx+2*x^2-6*x+6 u tocki x99=3:', fun(3))

#Greska
print('Greska za tocku x99=3: %.15f'% abs(yi[99,0]-fun(3)))

#Graf
xtocke=linspace(1,3,10)
plot(xtocke,fun(xtocke),'o',xi,yi[:,0],'-r')
grid()
show()