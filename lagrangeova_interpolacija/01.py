import pylab
import numpy as np
from numpy import linalg
from numpy.polynomial import polynomial as P

x = np.array([[1, 0, 0], [1, 2, 4], [1, 4, 16]])
y = np.array([3, 4, 2])

a = linalg.solve(x, y)

print(f'P(0) = {P.polyval(0., a)}')
print(f'P(2) = {P.polyval(2., a)}')
print(f'P(4) = {P.polyval(4., a)}')

x = np.linspace(.5, 5, 100)
y = P.polyval(x, a)

pylab.plot(x, y)
pylab.show()
