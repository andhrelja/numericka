import pylab
import numpy as np
from numpy import linalg
from numpy.polynomial import polynomial as P

# p(0) = 1 [[(0**0, 0**1, 0**2)],
# p(1) = 3 [(1**0, 1**1, 1**2)],
# p(4) = 0 [(4**0, 4**1, 4**2)]]

x = np.array([[1, 0, 0], [1, 1, 1], [1, 4, 16]])
y = np.array([1, 3, 0])

a = linalg.solve(x, y)

# P.polyval(x, P(y)) rješava polinom a
# tako što uvrštava dani x
print(f'P(0) = {P.polyval(0., a)}')
print(f'P(1) = {P.polyval(1., a)}')
print(f'P(4) = {P.polyval(4., a)}')

# x je polje od 100 elemenata gdje
# su elementi distriburiani od 0.5 do 5
x = np.linspace(0.5, 5, 100)
y = P.polyval(x, a)

pylab.plot(x, y)
pylab.show()
