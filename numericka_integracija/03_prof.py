# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 22:26:09 2019

@author: Math
"""

from numpy import *
import scipy.integrate as spi
from scipy import interpolate as i
from pylab import *

def f(x):
    return x**2-x+1

a = 1.0
b = 3.0
c = (a+b)/2.

x1 = array([a, b])
x2 = array([a, c, b])
y1 = f(x1)
y2 = f(x2)

iTrap = spi.trapz(y1, x1)
print('Rjesenje prema trapeznoj formuli je: ', iTrap)

iSimp = spi.simps(y2, x2)
print('Rjesenje prema Simpsonovoj formuli je: ', iSimp)

integral = 3**3/3-1**3/3-3**2/2+1**2/2+3-1
print('Prava vrijednost integrala je: ', integral)

print('Prava greska trapezne formule je: ', abs(integral-iTrap))
print('Prava greska Simpsonove formule je: ', abs(integral-iSimp))

pL=i.lagrange(x2, y2)
print('Kvadratni polinom kojim aproksimiramo u Simpsonovoj formuli je: ', pL)

xtocke = linspace(a, b, 50)
plot(xtocke, f(xtocke), x1, y1, xtocke, pL(xtocke))
fill_between(xtocke, f(xtocke), color='Magenta')
show()