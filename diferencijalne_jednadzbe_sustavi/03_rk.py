import numpy as np
import pylab
from scipy import integrate

f = lambda y, x: x - y
fun = lambda x: 2 * np.e**(x) - x - 1

x0 = 0.
y0 = 1.

n = 99
b = 1.
h = (b-x0)/n

def RungeKutta4(f, x, y, h):
    for i in range(n):
        k1 = f(x, y)
        k2 = f(x + h/2, y + k1*h/2)
        k3 = f(x + h/2, y + k2*h/2)
        k4 = f(x + h, y + k3*h)

        y += h*(k1 + 2*k2 + 2*k3 + k4)/6
        x += h
        print(x, y)
    return y

y99 = RungeKutta4(f, x0, y0, h)
print("Aproksimativno rjesenje je:", y99)