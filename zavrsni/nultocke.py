import numpy as np
import pylab

MAX_ITERACIJA = 50
EPS = 1e-10

"""
def f(x):
    return x - np.e**(-x)


def df(x):
    return 1 - np.e**(-x)


def d2f(x):
    return np.e**(-x)


"""
def f(x):
    return x**5 - x**3 - 2

def df(x):
    return 5*x**4 - 3*x**2

def d2f(x):
    return 20*x**3 - 6*x



def Bisekcija(a, b, i=1):
    print("{0}. iteracija:\n\t|-->f({1}) = {2}\n\t|-->f({3}) = {4}".format(i, a, f(a), b, f(b)))

    if i == MAX_ITERACIJA:
        print("Rješenje na zadanu točnost nije nađeno u zadanom broju iteracija.")
        return None
    
    x = (a + b) / 2.

    if abs(f(x)) < EPS or f(x) == 0.:
        return x, f(x)
    elif f(a) * f(x) < 0:
        b = x
    else:
        a = x
    
    return Bisekcija(a, b, i+1)


def Newton(a, i=1):
    if (f(a) * d2f(a)) > 0:
        return NewtonTangenta(a, i)
    else:
        print("Metoda ne konvergira prema jedinstvenoj nultočki")
        return None

def NewtonTangenta(a, i=1):
    print("{0}. iteracija:\n\t|-->f({1}) = {2}".format(i, a, f(a)))

    if abs(df(a)) < EPS:
        print("Horizontalna tangenta!")
        return None

    if i == MAX_ITERACIJA:
        print("Rješenje na zadanu točnost nije nađeno u zadanom broju iteracija.")
        return None

    x = a - (f(a) / df(a))
    if abs(x - a) < EPS:
        return x, f(x)
    else:
        return NewtonTangenta(x, i+1)


def Sekanta(a, b, i=1):
    if (f(a) * f(b)) < 0:
        return MetodaSekante(a, b, i)
    else:
        print("Metoda ne konvergira prema jedinstvenoj nultočki")
        return None

def MetodaSekante(a, b, i=1):
    print("{0}. iteracija:\n\t|-->f({1}) = {2}".format(i, b, f(b)))
    
    if i == MAX_ITERACIJA:
        print("Rješenje na zadanu točnost nije nađeno u zadanom broju iteracija.")
        return None
    
    x = b - (((b - a) / (f(b) - f(a)))*f(b))
    if abs(x - b) < EPS or f(x) == 0.:
        return x, f(x)
    else:
        return MetodaSekante(b, x, i+1)


if __name__ == '__main__':
    bisekcija = True
    newton = False
    sekanta = False

    a, b = -2, 2
    x = np.linspace(a, b, 100)

    pylab.grid()
    pylab.plot(x, f(x), 'r-')

    if bisekcija:
        rezultat = Bisekcija(a, b)
        legend = ["Bisekcija"]

        if rezultat is not None:
            print("x = {0}\nf(x) = {1}".format(rezultat[0], rezultat[1]))

            pylab.plot(*rezultat, 'bo')
            legend.append("Nultočka (x = {0})".format(round(rezultat[0], 2)))
    elif newton:
        rezultat = Newton(b)
        legend = ["Newton"]

        if rezultat is not None:
            print("x = {0}\nf(x) = {1}".format(rezultat[0], rezultat[1]))

            pylab.plot(*rezultat, 'bo')
            legend.append("Nultočka (x = {0})".format(round(rezultat[0], 2)))
    elif sekanta:
        rezultat = Sekanta(a, b)
        legend = ["Sekanta"]

        if rezultat is not None:
            print("x = {0}\nf(x) = {1}".format(rezultat[0], rezultat[1]))

            pylab.plot(*rezultat, 'bo')
            legend.append("Nultočka (x = {0})".format(round(rezultat[0], 2)))

    pylab.legend(legend)
    pylab.show()
