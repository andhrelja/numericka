
def spline(X, y, x):
    first = ((x - X[1]) / (X[0] - X[1])) * y[0]
    second = ((x - X[0]) / (X[1] - X[0])) * y[1]
    return first + second