import numpy as np
import pylab

x = [0, 2, 4, 6, 8, 10, 12, 16]
y = [25, 36, 52, 68, 85, 104, 142, 260]

def f(x):
    return a*np.exp(b*x)

lny = np.log(y)

koef = np.polyfit(x, lny, 1)
print("Koeficijenti regresijskog pravca: ", koef)

reg_pravac = np.poly1d(koef)
print("Regresijski pravac je: ", reg_pravac)

a = np.exp(reg_pravac[0])
b = reg_pravac[1]

print("Broj mikroorganizama nakon 18 sati je:", f(18.))

xtocke = np.linspace(min(x), max(x), 100)
yExp = f(xtocke)

pylab.plot(x, y, 'o', xtocke, yExp, 'r-')
pylab.show()
