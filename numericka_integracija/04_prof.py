# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 22:34:55 2019

@author: Math
"""

from numpy import *
import scipy.integrate as spi
from scipy import interpolate as i
from pylab import *

def f(x):
    return log(x)

def AbsD2f(x):
    return abs(-1./(x**2))

def AbsD4f(x):
    return abs(-6./(x**4))

a = 1.0
b = 3.0
n = 4
h = (b-a)/n
x = linspace(a, b, n+1)
y = f(x)

iPTrap = h/2*(y[0]+2*sum(y[1:4])+y[4])
print('Rjesenje putem produljene trapezne formule na prvi nacin je: ', iPTrap)

iTrap = spi.trapz(y, x)
print('Rjesenje putem produljene trapezne formule na drugi nacin je: ', iTrap)

iPSimp = 0.
for i in range(0, n-1, 2):
    iPSimp = iPSimp+h/3.*(y[i]+4*y[i+1]+y[i+2])
print('Rjesenje putem produljene Simpsonove formule na prvi nacin je: ', iPSimp)

iSimp = spi.simps(y, x)
print('Rjesenje putem produljene SImpsonove formule na drugi nacin je: ', iSimp)

integral = 3*log(3)-2
print('Prava vrijednost integrala je: ', integral)

print('Prava greska produljene trapezne formule je: ', abs(integral-iTrap))
print('Prava greska produljene Simpsonove formule je: ', abs(integral-iSimp))

xtocke = linspace(a, b, 100)
#plot(xtocke, AbsD2f(xtocke))

M2 = AbsD2f(1.)
print('Apsolutna greska produljene trapezne formule je <= ', (b-a)/12.*h**2*M2)

xtocke1 = linspace(a, b, 100)
#plot(xtocke, AbsD4f(xtocke1))

M4 = AbsD4f(1.)
print('Apsolutna greska produljene Simpsonove formule je <= ', (b-a)/180.*h**4*M4)

plot(xtocke, f(xtocke), x, y, 'r-', x, y, 'yo')
fill_between(xtocke, f(xtocke), color='LightBlue')
show()