# Vježbe 3. - Lagrangeov interpolacijski polinom
  
## 02.py
p(0) = 1; p(1) = 3; p(4) = 0
  
Običan polinom izgleda:
p2(x) = ax**2 + bx + c
p3(x) = ax**3 + bx**2 + cx + d  

Polinom definiramo kao numpy polje koje se sastoji od polja polinoma. Tako će se svako polje unutar polja polinoma sastojati od koeficijenata polinoma.
`x = np.array([[1, 0, 0], [1, 1, 1], [1, 4, 16]])`

