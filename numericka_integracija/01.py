import numpy as np
import scipy.integrate as spi
import scipy.interpolate as interpolate
import pylab

def f(x):
    return np.e ** x

a = 0.
b = 3.
c = (a+b)/2

x1 = np.array([a,b])
y1 = f(x1)

i_trap = spi.trapz(y1, x1)

print("Vrijednost integrala trapeznom formulom je: ", i_trap)

x2 = np.array([a, c, b])
y2 = f(x2)

i_simps = spi.simps(y2, x2)
print("Vrijednost integrala Simposonovom formulom je: ", i_simps)

integral = np.e ** 3 - 1
print("Egzaktna vrijednost integrala je: ", integral)
print("Prava greška trapezne formule je: ", abs(integral - i_trap))
print("Prava greška Simpsonove formule je: ", abs(integral - i_simps))

derivacija2 = lambda x: np.e ** x
M2 = max(derivacija2(a), derivacija2(b))
print("Apsolutna greška trapezne formule je <= ", ((b - a) ** 3 / 12)* M2)

derivacija4 = lambda x: np.e ** x
M4 = max(derivacija4(a), derivacija4(b))
print("Apsolutna greška Simpsonove formule je <= ", ((b - a)/2) ** 5 * (M4 / 90.))

lagrange_polinom = interpolate.lagrange(x2, y2)
print("Lagrangeov interpolacijski polinom je:\n", lagrange_polinom)

xtocke = np.linspace(a, b, 50)
pylab.plot(xtocke, f(xtocke), x1, y1, xtocke, lagrange_polinom(xtocke))
pylab.fill_between(xtocke, f(xtocke))
pylab.show()
