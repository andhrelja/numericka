# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 13:16:12 2019

@author: Korisnik
"""

import decimal as dec
import math

def f(n):
    if n == 1:
        return 1
    else:
        a = 0.5 * (f(n-1) + 2./f(n-1))
        return a

for i in range(1, 21):
    print "Clan rekurzije je jednak {}, a apsolutna greska je {}"\
    .format(dec.Decimal(f(i)), abs(dec.Decimal(math.sqrt(2)) - dec.Decimal(f(i))))