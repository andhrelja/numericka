import numpy as np
#import pylab


x = np.array([2014, 2013, 2012, 2011, 2010, 2009, 2008, 2007, 2006, 2005, 2004, 2003, 2002, 2001, 2000])
y = np.array([12.14, 12.39, 11.49, 12.19, 12.1, 12.21, 12.06, 12.56, 11.83, 11.59, 13.11, 13.78, 13.39, 13.15, 12.73])
#drugi_y = [2,4,6,8,9]

koef = np.polyfit(x, y, 2)
#koef = np.polyfit(x, drugi_y, 1)
print("Koeficijenti regresijskog pravca su:", koef)

reg_pravac = np.poly1d(koef)
print("Jednadžba regresijskog pravca je:", reg_pravac)

y_red_pravac = reg_pravac(x)

pylab.plot(x, y, 'o', x, y_red_pravac, 'm-')
pylab.xlabel("x - os")
pylab.ylabel("y - os")
pylab.show()
pylab.grid()
