import numpy as np
import pylab
from scipy import integrate

f = lambda z, y: np.array([z[1], 5*z[1] - 4*z[0]])
fun = lambda x: 4*np.e**x + np.e**(4*x)

y0 = np.array([5., 8.])
xi = np.linspace(0, 2, 100)
yi = integrate.odeint(f, y0, xi)

print("Pribiližna vrijednost:", yi[99,0])
print("Prava vrijednost:", fun(2.))
print("Apsolutna greška:", abs(yi[99,0] - fun(2.)))
