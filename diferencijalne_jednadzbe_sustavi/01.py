import numpy as np
import pylab
from scipy import integrate

f = lambda y, x: x - y
fun = lambda x: np.e**(-x+1) + x-1

x0 = 1.
y0 = 1.

xi = np.linspace(1, 3, 100)
yi = integrate.odeint(f, y0, xi)

print("Aproksimativne vrijednosti yi:")
for i in range(100):
    print(f"y[{i}] = {yi[i,0]}")

print("Egzaktna vrijednost funkcije u x=3 je:", fun(3.))
print("Aproksimativna vrijednost funkcije u x=3 je:", yi[99][0])
print("Apsolutna greška je:", abs(fun(3.) - yi[99][0]))

xtocke = np.linspace(1, 3, 100)

pylab.plot(xtocke, fun(xtocke), '-b', xi, yi, '--r')
pylab.show()
