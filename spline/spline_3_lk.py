import numpy as np
from scipy import interpolate
import pylab as pl

def f(x):
    return x * np.cos(x)

xcvorovi = np.genfromtxt("spline_3_lk.txt", dtype=float, delimiter=",")
ycvorovi = f(xcvorovi)

lspline = interpolate.interp1d(xcvorovi, ycvorovi, kind='linear')
kspline = interpolate.interp1d(xcvorovi, ycvorovi, kind='cubic')

print("Apsolutna greška za lspline u x = 1 je:", abs(f(1) - lspline(1)))
print("Apsolutna greška za kspline u x = 1 je:", abs(f(1) - kspline(1)))

xtocke = np.linspace(min(xcvorovi), max(xcvorovi), 100)
yfunkcija = f(xtocke)

y_lspline = lspline(xtocke)
y_kspline = kspline(xtocke)

pl.figure()
pl.plot(xcvorovi, ycvorovi, 'o', xtocke, y_lspline, '-r', xtocke, y_kspline, '-b', xtocke, yfunkcija, '--')
pl.legend(['Cvorovi', 'Lspline', 'Kspline', 'Funkcija'])
pl.grid()
pl.show()