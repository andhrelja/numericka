import datetime
import decimal as dec
dec.getcontext().prec = 45

suma1 = 0
start = datetime.datetime.now()


for i in range(1, 10**6):
    suma1 = dec.Decimal(suma1) + dec.Decimal(1. / i)
    
print dec.Decimal(suma1)

suma2 = 0
for i in range(10**6 + 1, 0, -1):
    suma2 = dec.Decimal(suma2) + dec.Decimal(1. / i)
    
print dec.Decimal(suma2)

end = datetime.datetime.now()

print "Apsolutna vrijednost razlike dviju suma je :", abs(dec.Decimal(suma1)) - dec.Decimal(suma2)
print "Vrijeme: ", end - start, "s"