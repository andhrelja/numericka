import numpy as np
import pylab

x = [15., 22., 25., 30., 40., 45., 50., 60., 70., 80., 95., 100., 120., 130., 150.]
y = [80., 95., 100., 120., 110., 145., 130., 180., 210., 200., 280., 320., 350., 375., 480]

koef = np.polyfit(x, y, 1)
print("Koeficijenti linearnog regresijskog pravca:", koef)

reg_pravac = np.poly1d(koef, variable='t')
print(f'Jednadžba linearnog regresijskog pravca:\nR(t) = {reg_pravac}')

print("Procijenjeni god. profit za ulog od 220 000€ je ", reg_pravac(220))
x.append(220)
y.append(reg_pravac(220))

y_reg_pravac = reg_pravac(x)

pylab.plot(x, y, 'o', x, y_reg_pravac, '--')
pylab.xlabel("x - os")
pylab.ylabel("y - os")
pylab.show()