import numpy as np
import pylab

def f(x):
    return x**2 + 4*np.sin(x) - 1

def df(x):
    return 2*x + 4*np.cos(x)

def d2f(x):
    return 2 - 4*np.sin(x)

#x = np.linspace(-3, 1, 100)
#pylab.plot(x, f(x), 'r-')
#pylab.grid()
#pylab.show()


def Sekanta(a, b, eps, max_iteracija=30):
    print("0.-ta iteracija je: %s" %a)
    print("1.-ta iteracija je: %s" %b)
    i = 1
    while abs(b-a) > eps:
        i += 1
        xi = b - (b-a)/(f(b)-f(a)) * f(b)
        print("%s. iteracija je: %s" %(i, xi))
        if f(xi) == 0:
            return xi, i
        else:
            a = b
            b = xi
        if i == max_iteracija:
            print("Rješenje do na zadanu točnost sa postavljenim brojem iteracija nije nađeno!")
            break
    return xi, i


def Newton(x, eps, max_iteracija=30):
    if f(x) * d2f(x) <= 0:
        print("Nema kovergencije. Birati drugi kraj segmenta.")
    else:
        print("0.-ta iteracija: %s " %x)
        i = 1
        while i <= max_iteracija:
            if abs(df(x)) < eps:
                print("Horizontalna tangenta!")
                break
            dx = f(x) / df(x)
            x = x - dx
            print("%s. iteracija je: %s" %(i, x))
            if abs(dx) < eps:
                return x, i
            else:
                i += 1
        print("Rješenje do na zadanu točnost nije određeno!")


def Bisekcija(a, b, eps, max_iteracija=30):
    xi = (a+b)/2.0
    i = 0
    print('%s. iteracija: %s' % (i, xi))
    while (b-a)/2.0 > eps:
        if f(xi) == 0:
            return xi,i
        elif f(a)*f(xi) < 0:
            b = xi           
        else:
            a = xi
        xi = (a+b)/2.0
        i = i+1
        print('%s. iteracija: %s' % (i, xi))
        if i == max_iteracija:
            print('Rjesenje na zadanu tocnost nije nadjeno u zadanom broju iteracija.')
            break 
    return xi, i


print("\nMetoda sekante:")
xn, i = Sekanta(-2.5, 2.0, 1.e-8)
print("x%s = %s" %(i, xn))

print("\n")
xm, j = Sekanta(0, 0.5, 1.e-8)
print("x%s = %s" %(j, xm))

#x = np.linspace(-3, 1, 100)
#pylab.plot(x, f(x), 'r-', xn, f(xn), 'go', xm, f(xm), 'bo')
#pylab.grid()
#pylab.show()

print("\nMetoda tangente:")
xn, i = Newton(-2.5, 1.e-8)
print("x%s = %s" %(i, xn))

print("\n")
xm, j = Newton(0.5, 1.e-8)
print("x%s = %s" %(j, xm))

#x = np.linspace(-3, 1, 100)
#pylab.plot(x, f(x), 'r-', xn, f(xn), 'go', xm, f(xm), 'bo')
#pylab.grid()
#pylab.show()

print("\nBisekcija:")
xn, i = Bisekcija(-2.5, -2, 1.e-8)
print('Rjesenje1:')
print('x%s=%s' % (i, xn))

print('\n')
xm, j = Bisekcija(0, 0.5, 1.e-8)
print('Rjesenje2:')
print('x%s=%s' % (j,xm))
