#Na intervalu [0,2] na 100 ekvidistantno udaljenih tocaka nadjite i prikazite
#graficki priblizno rjesenje obicne diferencijalne jednadzbe y''-5*y'+4*y=0.
#Koristite pocetne uvjete: y(0)=5, y'(0)=8. Usporedite numericko rjesenje
#s analitickim rjesenjem y=4*e^(x)+e^(4*x).

from scipy import integrate
from numpy import *
from pylab import *

#Funkcija kojom je definiran sustav diferencijalnih jednadzbi
def f(z,x):
    return array([z[1], 5*z[1]-4*z[0]])

#Analiticko rjesenje
def fun(x):
    return 4*exp(x)+exp(4*x)

#Pocetni uvjeti
y0=array([5.,8.])

#Određivanje 100 ekvidistantnih tocaka na segmentu [0,2]
xi=linspace(0,2,100)

#Rjesenje
yi=integrate.odeint(f,y0,xi,atol=1.e-12,rtol=1.e-12)
print(yi)
print('Aproksimativne vrijednosti yi:')
for i in range(0,100):
    print('y%d=%.15f' % (i,yi[i,0]))
print('Vrijednost funkcije y=4*e^(x)+e^(4*x) u tocki x99=2:', fun(2))

#Greska
print('Greska za tocku x99=2: %.15f'% abs(yi[99,0]-fun(2)))

#Graf
xtocke=linspace(0,2,10)
plot(xtocke,fun(xtocke),'o',xi,yi[:,0],'-r')
grid()
show()