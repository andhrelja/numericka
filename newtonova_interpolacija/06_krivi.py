import numpy as np
from math import factorial as fact

c = 0.
d = 1.
n = 4
h = (d-c)/n

x = list()

for i in range(n+1):
    x.append(c + i*h)

def f(x):
    return np.sin(x)

y = f(x)
m = len(x)

k = list()
for i in range(m):
    k.append(y[i])

for j in range(1, m):
    for i in range(m-1, j-1, -1):
        k[i] = float(k[i] - k[i-1])

a = list()

for i in range(m):
    a.append(k[i])

for i in range(1, m):
    a[i] = a[i] / fact(i) + h ** i

print("Koeficijenti NIP-a:", a)