import pylab
import numpy as np
from numpy.polynomial import polynomial as P

m1 = np.array([[1, 0, 0, 0],
               [1, np.pi, np.pi**2, np.pi**3],
               [0, 1, 0, 0],
               [0, 1, 2*np.pi, 3*np.pi**2]])
m2 = np.array([0, 0, 1, -1])

a = np.linalg.solve(m1, m2)
d = P.polyder(a)

print(f'P(0) = {P.polyval(0., a)}')
print(f'P(pi) = {P.polyval(np.pi, a)}')
print(f'P\'(0) = {P.polyval(0., d)}')
print(f'P\'(pi) = {P.polyval(np.pi, d)}')

x = np.linspace(0.5, 20, 100)
y = P.polyval(x, a)
z = np.sin(x)

pylab.plot(x, y, x, z)
pylab.show()