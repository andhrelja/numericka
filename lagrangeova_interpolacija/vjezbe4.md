# Vježbe 4. - Lagrangeov interpolacijski polinom

## 03.py
p(0) = 1; p'(0) = 1; p(1) = 1
Da bi rješili derivaciju polinoma, moramo njegove članove zapisati kao derivaciju polinoma.

Derivacija polinoma izgleda:
p'2(x) = ax + b + 0
p'3(x) = ax**2 + bx + c + 0

