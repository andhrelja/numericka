# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 22:14:08 2019

@author: Math
"""

from numpy import *
import scipy.integrate as spi
from scipy import interpolate as i
from pylab import *

def f(x):
    return x**2 + x

a = 0.0
b = 3.0
c = (a+b)/2.

x1 = array([a, b])
x2 = array([a, c, b])
y1 = f(x1)
y2 = f(x2)

iTrap = spi.trapz(y1, x1)
print('Trapeznom formulom dobivamo rjesenje: ', iTrap)

iSimp = spi.simps(y2, x2)
print('Simpsonovom formulom dobivamo rjesenje: ', iSimp)

integral = 3**3/3.+3**2/2.
print('Pravo rjesenje integrala je: ', integral)

M2 = 2

print('Apsolutna greska trapezne formule je <= ', (b-a)**3/12.*M2)
print('Prava greska Simpsonove formule je: ', abs(integral-iSimp))

pL=i.lagrange(x2, y2)
print('Lagrangeov polinom koji aproksimira f(x) je: ', pL)

xtocke=linspace(a, b, 50)
plot(xtocke, f(xtocke), x1, y1, xtocke, pL(xtocke), x2, y2)
fill_between(xtocke, f(xtocke),color='LightBlue')
show()