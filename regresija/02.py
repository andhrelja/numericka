import numpy as np
import pylab

x = [19.1, 25., 30.1, 36., 40., 45.1, 50.]
y = [76.3, 77.8, 79.75, 80.8, 82.35, 83.9, 85.1]

koef = np.polyfit(x, y, 1)
print("Koeficijenti linearnog regresijskog pravca:", koef)

reg_pravac = np.poly1d(koef, variable='t')
print(f'Jednadžba linearnog regresijskog pravca:\nR(t) = {reg_pravac}')

y_reg_pravac = reg_pravac(x)

pylab.plot(x, y, 'o', x, y_reg_pravac, '--')
pylab.xlabel("x - os")
pylab.ylabel("y - os")
pylab.show()