#Neka je funkcija f : R \to R zadana formulom f(x)=x^2+4*sin(x)-1.
#Odredite broj realnih nultocki funkcije te ih izracunajte s tocnoscu 10^(-8)
#koristeci metodu bisekcije i Newtonovu metodu.

import numpy as np
from pylab import *

def f(x):
    return x**2+4*sin(x)-1

x=linspace(-3,5,100)
plot(x, f(x), 'r-')
ylabel('y-os')
xlabel('x-os')
grid()
show()


#Iz grafa se vidi da postoje dvije nultocke:
#jedna je unutar segmenta [-2.5,-2], a druga unutar segmenta [0,0.5].

#Metoda bisekcije

print('Metoda bisekcije: \n')

def Bisekcija(a,b,eps,MaxIteracija=30):
    xi=(a+b)/2.0
    i=0
    print('%s. iteracija: %s' % (i,xi))
    while (b-a)/2.0>eps:
        if f(xi)==0:
            return xi,i
        elif f(a)*f(xi)<0:
            b=xi           
        else:
            a=xi
        xi=(a+b)/2.0
        i=i+1
        print('%s. iteracija: %s' % (i,xi))
        if i==MaxIteracija:
            print('Rjesenje na zadanu tocnost nije nadjeno u zadanom broju iteracija.')
            break 
    return xi,i

xn,i=Bisekcija(-2.5,-2,1.e-8)
print('Rjesenje1:')
print('x%s=%s' % (i,xn))
print('\n')
xm,j=Bisekcija(0,0.5,1.e-8)
print('Rjesenje2:')
print('x%s=%s' % (j,xm))
print('\n')

#Newtonova metoda

print('Newtonova metoda: \n')

def df(x):
    return 2*x+4*cos(x)
    
def d2f(x):
    return 2-4*sin(x)
    
def Newton(x,eps,MaxIteracija=20):
    if f(x)*d2f(x)<0:
        print('Newtonova metoda ne konvergira. Treba izabrati drugu pocetnu aproksimaciju.')
    else:
        print('0. iteracija: %s' % x)
        i=1
        while i<=MaxIteracija:
            if abs(df(x))<eps: 
                print('Horizontalna tangenta')
                break
            dx=f(x)/df(x)
            x=x-dx
            print('%s. iteracija: %s' % (i,x))
            if abs(dx)<eps:
                return x,i 
            else:
                i=i+1
        print('Rjesenje na zadanu tocnost nije nadjeno u zadanom broju iteracija.')
    

xn,i=Newton(-2.5,1.e-8)
print('Rjesenje1:')
print('x%s=%s' % (i,xn))
print('\n')
xm,j=Newton(0.5,1.e-8)
print('Rjesenje2:')
print('x%s=%s' % (j,xm))
print('\n')

#Metoda sekante

print('Metoda sekante: \n')

def Sekanta(a,b,eps,MaxIteracija=20):
    print('0. iteracija: %s' % a)
    print('1. iteracija: %s' % b)
    i=1
    while abs(b-a)>eps:
        i=i+1
        xi=b-f(b)*(b-a)/(f(b)-f(a))
        print('%s. iteracija: %s' % (i,xi))
        if f(xi)==0:
            return xi,i
        else:
            a=b
            b=xi        
        if i==MaxIteracija:
            print('Rjesenje na zadanu tocnost nije nadjeno u zadanom broju iteracija.')
            break
    return xi,i

xn,i=Sekanta(-2.5,-2,1.e-8)
print('Rjesenje1:')
print('x%s=%s' % (i,xn))
print('\n')

xm,j=Sekanta(0,0.5,1.e-8)
print('Rjesenje2:')
print('x%s=%s' % (j,xm))
print('\n')

x=linspace(-3,1,100)
plot(x, f(x), 'r-',xn,f(xn),'m^',xm,f(xm),'m^')
ylabel('y-os')
xlabel('x-os')
grid()
show()