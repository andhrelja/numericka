# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 12:59:38 2019

@author: Korisnik
"""
from datetime import datetime
import decimal as dec

dec.getcontext().prec = 45
suma1 = 0
suma2 = 0

start = datetime.now()

for i in range(1, 10**6):
    suma1 = dec.Decimal(suma1) + dec.Decimal(1. / i**2)
    
for i in range(10**6 + 1, 0, -1):
    suma2 = dec.Decimal(suma2) + dec.Decimal(1. / i**2)
    
end = datetime.now()
    
print "Vrijednost unaprijed: ", dec.Decimal(suma1)
print "Vrijednost unazad: ", dec.Decimal(suma2)
print "Apsolutna vrijednost greške:", abs(dec.Decimal(suma1) - dec.Decimal(suma2)) 
print "Vrijeme:", end-start