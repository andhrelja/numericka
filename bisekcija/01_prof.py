from numpy import *
from pylab import *

x = linspace(-2, 2, 100)
plot(x, x, 'r-', x, e**(-x), 'g-')
grid()
show()

def f(x):
    return x - e**(-x)

def Bisekcija(a, b, eps, MaxIteracija=30):
    xi=(a+b)/2.0
    i=0
    print('%s . iteracija: %s' % (i, xi))
    while (b-a)/2.0 > eps:
        if f(xi)==0:
            return xi, i
        elif f(a)*f(xi)<0:
            b=xi
        else:
            a=xi
        xi=(a+b)/2.0
        i=i+1
        print('%s . iteracija: %s' % (i, xi))
        if i==MaxIteracija:
            print('Nismo nasli nultocku do zadanu tocnost u zadanom broju iteracija')
            break
    return xi, i

xn,i=Bisekcija(0.5, 1.0, 1.e-8)
print('Rjesenje')
print('x%s=%s' % (i, xn))







        