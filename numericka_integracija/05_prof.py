# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 22:54:52 2019

@author: Math
"""

from numpy import *
import scipy.integrate as spi
from scipy import interpolate as i
from pylab import *

def f(x):
    return sin(x)

def AbsD2f(x):
    return abs(-(sin(x)))

a = 0.0
b = 3.0
eps = 1.e-4

#Racunanje broja podsegmenata na koji treba podijeliti pocetni segment [0, 3]
#Iz grafa trazimo M2

xtocke = linspace(a, b, 100)
plot(xtocke, AbsD2f(xtocke), pi/2., AbsD2f(pi/2.), 'yo')
grid()
show()

M2 = AbsD2f(pi/2.)
n = ceil((b-a)*sqrt((M2/eps)*((b-a)/12)))
print('Broj segmenata na koji treba podijeliti pocetni segment je: ', n)

x = linspace(a, b, 151)
y=f(x)
iTrap = spi.trapz(y, x)
print('Rjesenje produljenom trapeznom formulom na prvi nacin je: ', iTrap)

h = (b-a)/150.

iPTrap = h/2.*(y[0]+2*sum(y[1:150])+y[150])
print('Rjesenje produljenom trapeznom formulom na prvi nacin je: ', iPTrap)

integral = -cos(3.)+cos(0.)
print('Prava vrijednost integrala je: ', integral)
print('Apsolutna greska produljene trapezne formule je <= ', (b-a)/12.*h**2*M2)