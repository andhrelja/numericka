import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-2, 2, 100)
plt.plot(x, x, 'r-', x, np.e**(-x), 'g-')
plt.grid()
plt.show()

def f(x):
    return x - np.e**(-x)
    #return x**5 - x**3 - 2

def Bisekcija(a, b, eps, MaxIteracija=30):
    xi = (a+b)/2.0
    i = 0
    print('%s . iteracija: %s' % (i, xi))
    while (b-a)/2.0 > eps:
        if f(xi) == 0:
            return xi, i
        elif f(a)*f(xi) < 0:
            b = xi
        else:
            a = xi
        xi = (a+b)/2.0
        i += 1
        print('%s . iteracija: %s' % (i, xi))
        if i == MaxIteracija:
            print('Nismo nasli nultocku do zadanu tocnost u zadanom broju iteracija')
            break
    return xi, i

xn, i = Bisekcija(0.5, 1, 1.e-8)
print('Rjesenje')
print('x%s=%s' % (i, xn))

#print(Bisekcija(1., 1.5, 1.e-3))