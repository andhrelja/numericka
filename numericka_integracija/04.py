import numpy as np
import scipy.integrate as spi
import scipy.interpolate as interpolate
import pylab

f = lambda x: np.log(x)
d2f = lambda x: abs(-1. / x**2)
d4f = lambda x: abs(-6. / x**4)

a, b = 1.0, 3.0
n = 4
h = (b - a)/n

# 5 točaka (4 podsegmenta)
x = np.linspace(a, b, n+1)
y = f(x)

# Formula sa slajda (produljna trapezna formula)
i_ptrap = h/2 * (y[0] + 2*sum(y[1:4]) + y[4])
print("Produljenom trapeznom formulom dobivamo rezultat: ", i_ptrap)

i_trap = spi.trapz(y, x)
print("Produljenom trapeznom formulom (gotovom funkcijom) dobivamo rezultat: ", i_trap)

i_psimp = 0.
for i in range(0, n-1, 2):
    i_psimp += h/3 * (y[i] + 4*y[i+1] + y[i+2])

print("Produljenom Simposonovom formulom dobivamo rezultat: ", i_psimp)

i_simp = spi.simps(y, x)
print("Produljenom Simposonovom formulom (gotovom funkcijom) dobivamo rezultat: ", i_simp)
