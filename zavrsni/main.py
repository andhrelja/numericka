import os
import numpy as np
from controller.LifeExpectancyController import LifeExpectancyController


COUNTRY = 'Croatia'

X = 'year'
Y = 'alcohol'
regresijski_pravac = True

def regression_export(lec):
    export_path = os.path.join('data', f'[Regression-{Y}]{COUNTRY}_Export.csv')
    lec.regression_update_column(X, Y)
    lec.export(export_path)

def lagrange_export(lec):
    export_path = os.path.join('data', f'[Lagrange-{Y}]{COUNTRY}_Export.csv')
    lec.lagrange_update_column(X, Y)
    lec.export(export_path)

def newton_export(lec):
    export_path = os.path.join('data', f'[Newton-{Y}]{COUNTRY}_Export.csv')
    lec.newton_update_column(X, Y)
    lec.export(export_path)


if __name__ == '__main__':
    import_path = os.path.join('data', 'Croatia_Edit.csv')

    life_expectancy_controller = LifeExpectancyController()
    life_expectancy_controller.read_data(import_path)
    life_expectancy_controller.filter_data(COUNTRY)
    life_expectancy_controller.sort_models(X)
    
    # life_expectancy_controller.add_years(COUNTRY, ['2016', '2017'])

    life_expectancy_controller.newton_update_column(X, Y)
    # life_expectancy_controller.lagrange_update_column(X, Y)
    # life_expectancy_controller.spline_update_column(X, Y)
    # life_expectancy_controller.regression_update_column(X, Y)
    
    #life_expectancy_controller.print_column(X, Y) 
    life_expectancy_controller.print_approximations()
    if regresijski_pravac:
        life_expectancy_controller.plot_regression_columns(COUNTRY, X, Y)
    else:
        life_expectancy_controller.plot_columns(COUNTRY, X, Y)