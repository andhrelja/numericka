import numpy as np
from scipy import interpolate
import pylab as pl

def f(x):
    return x ** 2 * np.sin(x)

xcvorovi = np.linspace(-np.pi, np.pi, 20)
ycvorovi = f(xcvorovi)

print(xcvorovi)
print(ycvorovi)

lspline = interpolate.interp1d(xcvorovi, ycvorovi)
print("Apsolutna greška u x = 1 je:", abs(lspline(1) - f(1)))

xtocke = np.linspace(min(xcvorovi), max(xcvorovi), 100)

yspline = lspline(xtocke) # aprokcimacija
yfunkcija = f(xtocke) # egzaktna funkcija

pl.figure()
pl.plot(xcvorovi, ycvorovi, 'o', xtocke, yspline, '-r', xtocke, yfunkcija, '--')
pl.legend(['Cvorovi', 'Lspline', 'Funkcija'])
pl.grid()
pl.show()
