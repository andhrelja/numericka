import numpy as np
from numpy.polynomial import polynomial as P
import pylab


def Lagrange(X, Y, x):
    n = len(X)
    result = 0

    l = lambda X, x, i: [((x - X[j]) / (X[i] - X[j]))
                          if i != j else 1 for j in range(n)]

    for i in range(n):
        result += Y[i] * np.product(l(X, x, i))

    return round(result, 2)


def PodijeljeneRazlike(X, Y):
	if len(X) == 1:
		return Y[0]

	first_x, first_y = X[1:], Y[1:]
	last_x, last_y = X[:-1], Y[:-1]

	return (PodijeljeneRazlike(first_x, first_y) - PodijeljeneRazlike(last_x, last_y)) / (first_x[len(first_x) - 1] - last_x[0])


def Newton(X, Y, x):
	n = len(X)
	result = 0

	for i in range(n):
		x_subset, y_subset = X[:i+1], Y[:i+1]
		prod = [(x - X[j]) for j in range(0, i)]
		result += PodijeljeneRazlike(x_subset, y_subset) * np.product(prod)

	return round(result, 2)


def InterpolationCoeff(A, B):
	return np.linalg.solve(A, B)


def LinearSpline(X, y, x):
	if len(X) != 2:
		print("Interpolacija linearnim splineom zahtjeva polje veličine 2")
		return None
	else:
		first = ((x - X[1]) / (X[0] - X[1])) * y[0]
		second = ((x - X[0]) / (X[1] - X[0])) * y[1]
		return first + second


def postaviLinearniSpline(x, y):
	n = len(x) - 1
	Y = list()

	for i in range(n):
		a = np.array([x[i], x[i+1]])
		b = np.array([y[i], y[i+1]])

		Y.append(y[i])
		Y.append(LinearSpline(a, b, x[i]+1))
		if i == n - 1:
			Y.append(y[n])
	
	return np.linspace(-10, 10, 21), np.array(Y)

def postaviInterpolacijskeKoeficijente():
	"""
	p(50) = 1
	p(350) = 5
	p(600) = 10

	A*X=B
	X = (A**-1)B
	"""
	A = np.array([
		[1, 50, 2500],
		[1, 350, 122500],
		[1, 600, 360000]
	])

	B = np.array([1, 5, 10])

	poly = InterpolationCoeff(A, B)

	pylab.figure()
	pylab.xlabel("Price")
	pylab.ylabel("Quantity")
	pylab.grid()

	x = np.linspace(0, 2600, 100)
	pylab.plot(x, P.polyval(x, poly))


if __name__ == '__main__':
	x = np.linspace(-10, 10, 11)
	f = lambda x: x**5 - x**3 + 2

	mysplines = postaviLinearniSpline(x, f(x))

	pylab.figure()
	pylab.grid()
	pylab.plot(*mysplines, '-', mysplines[0], f(mysplines[0]), 'o', mysplines[0], lsplines(mysplines[0]), 'r--')
	pylab.legend(["My Splines", "Function Values", "Scipy Splines"])
	pylab.show()
