from model.LifeExpectancyModel import LifeExpectancy
from najmanji_kvadrati import najmanji_kvadrati
from interpolacija import lagrange, newton
from regresija import regresijski_pravac
from spline import spline
import numpy as np
import pylab


class LifeExpectancyController:
    def __init__(self):
        self.head = str()
        self.models = list()
        self.approximations = list()
    
    def read_data(self, path):
        # Read CSV and load the model
        with open(path, 'r') as f:
            lines = f.readlines()
        
        self.head = lines[0].strip()
        head_list = self.head.split(",")
        for i in range(1, len(lines)):
            kwargs = dict()
            line = lines[i].strip().split(",")

            for j in range(len(head_list)):
                key = head_list[j].strip().lower()
                key = key.replace(" ", "_")
                key = key.replace("-", "_")
                key = key.replace("/", "_")
                key = key.replace("__", "_")
                kwargs[key] = line[j]

            life_expectancy = LifeExpectancy(**kwargs)
            self.models.append(life_expectancy)
    
    def filter_data(self, country):
        models = list()
        for model in self.models:
            if model.country == country:
                models.append(model)
        self.models = models

    def add_years(self, country, years):
        for year in years:
            life_expectancy = LifeExpectancy(country=country, year=year)
            self.models.append(life_expectancy)
        
        self.sort_models('year')

    def get_column(self, column_name):
        # Return column by column name
        lst = list()
        for model in self.models:
            try:
                item = getattr(model, column_name)
            except AttributeError:
                print("Attribute Error: ", column_name)
                exit()
            else:
                if item == '':
                    item = None
            lst.append(item)
        return lst


    def get_clean_columns(self, X_name, Y_name):
        # Return columns with non-empty values
        X = self.get_column(X_name)
        Y = self.get_column(Y_name)

        Y_full = list()

        for i in range(len(X)):
            if Y[i] is not None and X[i] is not None:
                Y_full.append(i)
        
        x = [X[i] for i in Y_full]
        y = [Y[i] for i in Y_full]

        return x, y
    

    def regression_update_column(self, X_name, Y_name):
        print("Approximation method: Quadratic Regression")

        X, Y = self.get_clean_columns(X_name, Y_name)
        F = regresijski_pravac(X, Y)

        # Update empty columns with regression approximated values
        for model in self.models:
            # Check for empty values and create an approximation
            if getattr(model, Y_name) == "":
                x = getattr(model, X_name)
                value = F(x)
                self.approximation_created(X_name, Y_name, x, value)
                setattr(model, Y_name, round(value, 2))
    
    def lagrange_update_column(self, X_name, Y_name):
        print("Approximation method: Lagrange Interpolation")

        X, Y = self.get_clean_columns(X_name, Y_name)
        F = lagrange

        # Update empty columns with interpolation approximated values
        for model in self.models:
            if getattr(model, Y_name) == "":
                x = getattr(model, X_name)
                X_i, Y_i = self.get_interpolation_rows(X, Y, x)
                value = F(X_i, Y_i, x)
                self.approximation_created(X_name, Y_name, x, value)
                setattr(model, Y_name, value)

    def newton_update_column(self, X_name, Y_name):
        print("Approximation method: Newton Interpolation")

        X, Y = self.get_clean_columns(X_name, Y_name)
        F = newton

        # Update empty columns with interpolation approximated values
        for model in self.models:
            if getattr(model, Y_name) == "":
                x = getattr(model, X_name)
                X_i, Y_i = self.get_interpolation_rows(X, Y, x)
                value = F(X_i, Y_i, x)
                self.approximation_created(X_name, Y_name, x, value)
                setattr(model, Y_name, value)
    
    def spline_update_column(self, X_name, Y_name):
        print("Approximation method: Linear Spline")

        X, Y = self.get_clean_columns(X_name, Y_name)
        F = spline

        # Update empty columns with spline approximated values
        for model in self.models:
            if getattr(model, Y_name) == "":
                x = getattr(model, X_name)
                X_i, Y_i = self.get_spline_rows(X, Y, x)
                value = F(X_i, Y_i, x)
                self.approximation_created(X_name, Y_name, x, value)
                setattr(model, Y_name, value)

    def get_interpolation_rows(self, X, Y, x):
        dictionary = { int(X[i]): Y[i] for i in range(len(X))}
        x = int(x)
        r = range(x-2, x+2)
        X = [key for key in dictionary.keys() if key in r]
        Y = [dictionary[key] for key in dictionary.keys() if key in r]
        return X, Y

    def get_spline_rows(self, X, Y, x):
        dictionary = { X[i]: Y[i] for i in range(len(X))}
        lesser_keys = [key for key in sorted(dictionary.keys()) if key < x]
        greater_keys = [key for key in sorted(dictionary.keys()) if key > x]

        try:
            a = lesser_keys[-1]
        except IndexError:
            a = greater_keys[1]
        
        try:
            b = greater_keys[0]
        except IndexError:
            b = lesser_keys[-2]

        X = [a, b]
        Y = [dictionary[key] for key in X]
        return X, Y

    def approximation_created(self, X_name, Y_name, x, y):
        self.approximations.append({
            X_name: x,
            Y_name: y
        })
    
    def print_approximations(self):
        if self.approximations:
            tail = list()
            print("Approximated values:")
            for approximation in self.approximations:
                head = [key for key in approximation.keys()]
                tail.append([str(approximation[key]) for key in approximation.keys()])
            print("\t|".join(head))
            tail_string = str()
            for t in tail:
                tail_string += "\t|".join(t) + "\n"
            print(tail_string)
        else:
            print("No approximations were made")

    def plot_columns(self, title, X_name, Y_name):
        # X, Y = self.get_clean_columns(X_name, Y_name)
        X = self.get_column(X_name)
        Y = self.get_column(Y_name)

        pylab.figure()
        pylab.plot(X, Y)
        pylab.xlabel(X_name)
        pylab.ylabel(Y_name)
        pylab.title(title)
        pylab.xticks(np.arange(min(X), max(X), 2.0))
        # pylab.legend([Y_name])
        pylab.grid()
        pylab.show()
    
    def plot_regression_columns(self, title, X_name, Y_name):
        # X, Y = self.get_clean_columns(X_name, Y_name)
        X = self.get_column(X_name)
        Y = self.get_column(Y_name)
        F = regresijski_pravac(X, Y)

        pylab.figure()
        pylab.plot(X, Y, 'o', X, F(X), '-')
        pylab.xlabel(X_name)
        pylab.ylabel(Y_name)
        pylab.title(title)
        pylab.xticks(np.arange(min(X), max(X), 2.0))
        # pylab.legend([Y_name])
        pylab.grid()
        pylab.show()

    def print(self):
        # Print all data
        print(self.head)
        for row in self.models:
            print(row)
            print("\n")

    def print_column(self, X_name, Y_name):
        print(X_name, "\t|", Y_name)
        # Print column data
        for model in self.models:
            X_value = getattr(model, X_name)
            Y_value = getattr(model, Y_name)
            string = f'{X_value}\t|{Y_value}'
            print(string)
    
    def sort_models(self, X_name):
        models = list()
        xs = filter(None, self.get_column(X_name))
        xs = sorted(xs)

        for x in xs:
            for model in self.models:
                model_attr = getattr(model, X_name)
                if model_attr == x:
                    models.append(model)
        self.models = models

    def export(self, path):
        # Export CSV lines
        string = self.head + "\n"
        for model in self.models:
            string += model.export_line()
        
        with open(path, 'w') as f:
            f.write(string)