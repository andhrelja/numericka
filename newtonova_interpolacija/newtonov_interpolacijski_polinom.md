# Newtonov interpolacijski polinom

## Vježba 3.

p(0) = -1  
- x0 = 0, f(x0) = -1   
p(2) = 2  
- x1 = 2, f(x1) = 2  
p(6) = 3  
- x2 = 6, f(x2) = 3  

0   -1(a0)  f(x1) - f(x0) / x1 - x0 = 3/2 = f[x0, x1](a1)   f[x1, x2] - f[x0, x1] / x2 - x0 = -5/24 = f[x0, x1, x2](a2)
2   2   f(x2) - f(x1) / x2 - x1 = 1/4 = f[x1, x2]
6   3   
  
p2(x) = -1 + f[x0, x1]*(x - 0) + f[x0, x1, x2]*(x - 0)(x + 1)   ** formula iz prezentacije**
  
## Vježba 4.

