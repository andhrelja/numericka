import numpy as np


def regresijski_pravac(X, y, deg=2):
    koef = np.polyfit(X, y, deg)
    reg_pravac = np.poly1d(koef)
    return reg_pravac