Razlika između podijeljenih i konačnih razlika je ta što konačne razlike imaju ekvidistantne čvorove kao ulazne podatke.
Formula ocjene greške Lagrangeovog i Newtonovog polinoma je ista.

DZ: Zadatak 1., Zadatak 2. 
f(x) = xcos(pix)m [-1,1]

f(-1) = -cos(-pi) = 1
f(0)  = 0
f(1)  = cos(pi) = -1

p2(x) = ax**2 + bx + c 