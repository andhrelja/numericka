# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 13:43:41 2019

@author: Korisnik

Slide 23
"""

import math

r = 7.5
delta_r = 0.05

a = 30.
delta_a = 0.1

def V(a, r):
    return 2 * a * r**2 * math.pi**2

def V_r(a, r):
    return 4 * a * r * math.pi**2

def V_a(a, r):
    return 2 * r**2 * math.pi**2

def AbsV(delta_a, delta_r):
    return abs(V_a(a, r)) * delta_a + abs(V_r(a, r)) * delta_r

def Rel(delta_a, delta_r):
    return (abs(V_a(a,r)) * delta_a + abs(V_r(a,r) * delta_r)) / V(a,r)

print("Apsolutna greška (apsolutna vrijednost greške) je:", AbsV(delta_a, delta_r))
print("Relativna vrijednost greške je:", Rel(delta_a, delta_r))