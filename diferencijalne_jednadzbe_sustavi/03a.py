#Na intervalu [0,1] na 100 ekvidistantno udaljenih tocaka Eulerovom metodom
#nadjite i prikazite graficki priblizno rjesenje obicne diferencijalne jednadzbe
#y'-y=x. Koristite pocetni uvjet y(0)=1. Usporedite numericko rjesenje
#s analitickim rjesenjem y=2*e^(x)-x−1.
#Nakon toga zadatak rijesite Runge-Kutta metodom 4. reda.

from scipy import integrate
from numpy import *
from pylab import *

#Funkcija kojom je definirana diferencijalna jednadzba
def f(y,x):
    return x+y

#Analiticko rjesenje    
def fun(x):
    return 2*e**x-x-1

#Određivanje 100 ekvidistantnih tocaka na segmentu [0,1]
x0=0.
b=1.
n=99          #broj podsegmenata
h=(b-x0)/n    #duljina svakog podsegmenta

#Eulerova metoda
xi=linspace(0,1,n+1)
yi=zeros(n+1)
yi[0]=1.        #iz pocetnog uvjeta
for i in range(1,n+1):
    yi[i]=yi[i-1]+h*f(yi[i-1],xi[i-1])

print('Aproksimativna vrijednost y99 dobivena pomocu Eulerove metode:', yi[99])  
print('Vrijednost funkcije y=2*e^x-x-1 u tocki x99=1:', fun(1))

#Greska
print('Greska za tocku x99=1:', abs(yi[99]-fun(1)))

#Graf
xtocke=linspace(0,1,10)
plot(xtocke,fun(xtocke),'o',xi,yi)
grid()
show()