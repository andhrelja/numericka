# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 23:25:51 2019

@author: Math
"""

from numpy import *
import scipy.integrate as spi
from pylab import *
from scipy import interpolate as i

def f(x):
    return sin(x)

def AbsD4f(x):
    return abs(sin(x))

a = 0.0
b = 3.0
eps = 1.e-4

#Iz grafa trazimo M4

xtocke = linspace(a, b, 100)
plot(xtocke, AbsD4f(xtocke), pi/2, AbsD4f(pi/2), 'yo')
grid()
show()

M4 = AbsD4f(pi/2)
n = ceil((b-a)*((M4*(b-a))/(180*eps))**(1./4.))

if n%2!=0:
    print('Dobili smo neparan broj podsegmenata pa dodajemo jos jedan podsegment')
    m = int(n+1)
    print('Broj potrebnih segmenata je: ', m)
else:
    m = int(n)
    print('Potrebni broj segmenata je: ', m)
    
x = linspace(a, b, m+1)
y = f(x)
iSimp = spi.simps(y, x)
print('Rjesenje integrala produljenom Simpsonovom formulom je: ', iSimp)

#n=12
h = (b-a)/m
iPSimp = 0.
for i in range(0, m-1, 2):
    iPSimp = iPSimp + h/3.*(y[i]+4*y[i+1]+y[i+2])
print('Rjesenje integrala produljenom Simpsonovom formulom je: ', iPSimp)

integral = -cos(3.)+cos(0.)
print('Pravo rjesenje integrala je: ', integral)
print('Apsolutna greska produljene Simpsonove formule je <= ', (b-a)/180.*h**4*M4)
    
