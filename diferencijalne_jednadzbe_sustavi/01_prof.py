#Na intervalu [1,3] na 100 ekvidistantno udaljenih tocaka nadjite i
#prikazite graficki priblizno rjesenje obicne diferencijalne jednadzbe
#y'+y=x. Koristite pocetni uvjet y(1)=1. Usporedite numericko rjesenje
#s analitickim rjesenjem y=e^(-x+1)+x-1.


from scipy import integrate
from numpy import *
from pylab import *

#Funkcija kojom je definirana diferencijalna jednadzba
def f(y,x):
    return x-y

#Analiticko rjesenje    
def fun(x):
    return e**(-x+1)+x-1.
  
#Pocetni uvjet  
x0=1.
y0=1.

#Određivanje 100 ekvidistantnih tocaka na segmentu [1,3]
xi=linspace(1,3,100)

#Rjesenje
yi=integrate.odeint(f,y0,xi,atol=1.e-12,rtol=1.e-12)
print('Aproksimativne vrijednosti yi:')
for i in range(0,100):
    print('y%d=%.15f' % (i,yi[i]))
print('Vrijednost funkcije y=e^(-x+1)+x-1 u tocki x99=3:', fun(3))

#Greska
print('Greska za tocku x99=3: %.15f'% abs(yi[99]-fun(3)))

#Graf
xtocke=linspace(1,3,10)
plot(xtocke,fun(xtocke),'o',xi,yi)
grid()
show()