#Neka je funkcija f:R \to R zadana formulom f(x) = x^5-x^3-2.
#Odredite broj realnih nultocki funkcije te ih izracunajte s tocnoscu 10^(-3)
#koristeci metodu bisekcije i Newtonovu metodu.

import numpy as np
from pylab import *

def f(x):
    return x**5-x**3-2

x=linspace(-2,2,100)
plot(x, f(x), 'r-')
ylabel('y-os')
xlabel('x-os')
grid()
show()

#Iz grafa se vidi da je nultocka unutar segmenta [1,1.5].

#Metoda bisekcije
#a=1, b=1.5

print('Metoda bisekcije: \n')

def Bisekcija(a,b,eps,MaxIteracija=20):
    xi=(a+b)/2.0
    i=0
    print('%s. iteracija: %s' % (i,xi))
    while (b-a)/2.0>eps:
        if f(xi)==0:
            return xi,i
        elif f(a)*f(xi)<0:
            b=xi           
        else:
            a=xi
        xi=(a+b)/2.0
        i=i+1
        print('%s. iteracija: %s' % (i,xi))
        if i==MaxIteracija:
            print('Rjesenje na zadanu tocnost nije nadjeno u zadanom broju iteracija.')
            break 
    return xi,i

xn,i=Bisekcija(1,1.5,1.e-3)
print('Rjesenje:')
print('x%s=%s' % (i,xn))
print('\n')

#Newtonova metoda

print('Newtonova metoda: \n')

def df(x):
    return 5*x**4-3*x**2
    
def d2f(x):
    return 20*x**3-6*x
    
def Newton(x,eps,MaxIteracija=20):
    if f(x)*d2f(x)<0:
        print('Newtonova metoda ne konvergira. Treba izabrati drugu pocetnu aproksimaciju.')
    else:
        print('0. iteracija: %s' % x)
        i=1
        while i<=MaxIteracija:
            if abs(df(x))<eps: 
                print('Horizontalna tangenta')
                break
            dx=f(x)/df(x)
            x=x-dx
            print('%s. iteracija: %s' % (i,x))
            if abs(dx)<eps:
                return x,i 
            else:
                i=i+1
        print('Rjesenje na zadanu tocnost nije nadjeno u zadanom broju iteracija.')
    

xn,i=Newton(1.5,1.e-3)
print('Rjesenje:')
print('x%s=%s' % (i,xn))

x=linspace(-2,2,100)
plot(x, f(x), 'r-',xn,f(xn),'m^')
ylabel('y-os')
xlabel('x-os')
grid()
show()